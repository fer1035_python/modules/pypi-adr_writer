----
- Status: 
- Date: January 11, 2024 00:39:44 (UTC)
- Decider: Jane Doe
- Author: John Smith
- Deadline: January 31st, 2024
- Budget: 0.00
----

# ADR Writer

## Description

Produce ADRs based on discussions on technical problems and their solutions.

## Problem Statement

Currently, we create Architecture Decision Records (ADR) manually by editing Markdown templates. This can be error-prone, and we need to find a more automated way to do this.

## Considered Options

- Automation script
- Web form

## Decision Outcome

Automation script
- Script is already done.
- Total overhead is smaller.
- We don't have to worry about infrastructure.

### Consequences

- Technical solutioning discussions can be better facilitated with a more consistent thought process.
- ADRs can be produced faster and with a more consistent formatting.

## Pros and Cons of the Options

### Automation script

- Platform: Python
- Platform available: True
- Pros
	- Platform freely available.
	- Overhead is small.
	- Solution is portable across devices.
	- Record format consistency can be maintained easily.
- Cons
	- Need to write the script.

### Web form

- Platform: Dynamic hosting
- Platform available: False
- Pros
	- Prettier interface.
	- Shallower learning curve.
- Cons
	- Overhead is medium.
	- Need to setup hosting platform.
	- Need to write frontend and backend.


## More Information


## Alternative Considerations


