==============
**adr_writer**
==============

Overview
--------

Create Architecture Decision Records by answering questions to ensure consistency in formatting and thought processes.

Usage
-----

Installation:

.. code-block:: BASH

    pip3 install adr_writer
    # or
    python3 -m pip install adr_writer

Bash:

.. code-block:: BASH

    python </oath/to>/adr_writer

Python:

.. code-block:: PYTHON

    import adr_writer
    adr_writer.main()

Examples
--------

- `Execution <https://github.com/fer1035/pypi-adr_writer/blob/main/examples/execution.txt>`_
- `Record output <https://github.com/fer1035/pypi-adr_writer/blob/main/examples/0012-adr-writer.md>`_

Workflow
--------

.. image:: https://raw.githubusercontent.com/fer1035/pypi-adr_writer/main/images/workflow.png
    :alt: Workflow diagram
